# Cloud

## Type of cloud

#### Public Cloud

* aws
* azure
* gcp

#### Private Cloud

* kubernetes (k8s)
* openshift


#### Hybrid Cloud

a combination of the previous two


## Type of Machines

* bare metal
* virtual machine
* dedicated server
* container



Linux cgroup: https://www.youtube.com/watch?v=z7mgaWqiV90


## Cloud Native Company & Members

* redhat
* Hashicorp
* Weavework: https://www.weave.works/

* genisis cloud: https://github.com/genesiscloud
* stack.io: https://www.stack.io/services/monitoring