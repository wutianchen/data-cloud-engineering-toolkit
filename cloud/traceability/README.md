# Traceability

* events
* what is a span in terms of traceability
* trace: a collection of spans


## Videos

* [Traces from Events: A New Way to Visualise Kubernetes Activities - Bryan Boreham, Weaveworks](https://www.youtube.com/watch?v=g5tHHD4crtQ&t=94s)


## Tools

* [jaeger](https://www.jaegertracing.io/) 
* [kspan](https://github.com/weaveworks-experiments/kspan)