# IaC (Infrastructure as Code)

automation of infrastructure deployment, management, lifecycle

## Vendor-based

* [pulumi](https://www.pulumi.com/)
* [serverless framework](https://www.serverless.com/)
* [spacelift](https://spacelift.io/)
* [Sparkleformation](https://www.sparkleformation.io/)

## Configuration-based

* [ansible](https://www.ansible.com/)


## Hashicorp-based
* [openTofu](https://opentofu.org/): a fork of terraform, open sourced, community driven
* [hashicorp terraform](https://www.terraform.io/)
* [openbao](https://openbao.org/): open source and community version of vault
* [hashicorp vault](https://www.vaultproject.io/)


## Provider-based

* [aws cloudformation](https://aws.amazon.com/cloudformation/)
* [aws cdk](https://github.com/aws/aws-cdk)

## k8s-based

* [AWS Controllers for Kubernetes(ACK)](https://aws-controllers-k8s.github.io/community/docs/community/overview/)


