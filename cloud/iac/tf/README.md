# Terraform and OpenTofu


## Formatting and Linting

* [tflint](https://github.com/terraform-linters/tflint)

https://spacelift.io/blog/what-is-tflint

* [terraform fmt](https://developer.hashicorp.com/terraform/cli/commands/fmt)
* [terraform validate](https://developer.hashicorp.com/terraform/cli/commands/validate)
* [terrascan](https://github.com/tenable/terrascan)

https://spacelift.io/blog/what-is-terrascan

* [checkov](https://github.com/bridgecrewio/checkov)

https://spacelift.io/blog/what-is-checkov