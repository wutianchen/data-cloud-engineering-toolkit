# Terraform

* [terragrunt](https://terragrunt.gruntwork.io/)
* [gruntwork](https://www.gruntwork.io/)
* [cloudposse](https://github.com/cloudposse)
* [Your Weekly Dose of Terraform - Live Streams](https://medium.com/@anton.babenko/your-weekly-dose-of-terraform-live-streams-1d22c7acd3cb)
* [Awesome Terraform](https://github.com/shuaibiyy/awesome-terraform/blob/master/README.md#terraform-enterprise)

* [QuiNovas](https://www.quinovas.com/blog/)
* [GetCloudnative](https://github.com/getcloudnative)
* [camptocamp](https://github.com/camptocamp)
* [manage secrets in terraform code](https://blog.gruntwork.io/a-comprehensive-guide-to-managing-secrets-in-your-terraform-code-1d586955ace1)


## Formatting and Linting

* [tflint](https://github.com/terraform-linters/tflint)

https://spacelift.io/blog/what-is-tflint

* [terraform fmt](https://developer.hashicorp.com/terraform/cli/commands/fmt)
* [terraform validate](https://developer.hashicorp.com/terraform/cli/commands/validate)
* [terrascan](https://github.com/tenable/terrascan)

https://spacelift.io/blog/what-is-terrascan

* [checkov](https://github.com/bridgecrewio/checkov)

https://spacelift.io/blog/what-is-checkov


## tools
https://medium.com/env0/top-terraform-tools-to-know-in-2024-a00a232bb936

* terratest: https://github.com/gruntwork-io/terratest
* terraboard: https://github.com/devops-workflow/terraboard
* gruntworks-toc: https://github.com/gruntwork-io/toc
* tfmask: https://github.com/cloudposse/tfmask
* terrahub: https://www.terrahub.io/blog/terrahub-cli-is-open-source-and-free-forever/
* terraform-shell-resource: https://github.com/matti/terraform-shell-resource
* tfenv: http://blog.bennycornelissen.nl.s3-website-eu-west-1.amazonaws.com/post/managing-terraform-versions-like-a-pro/
* tfsec: https://github.com/aquasecurity/tfsec?ref=lindbergtech.com


Certification

* [My road to Hashicorp Terraform Associate](https://devopslearning.medium.com/my-road-to-hashicorp-terraform-associate-22f2e603ea2d)
* [21 Days of AWS using Terraform — Day 21](https://devopslearning.medium.com/21-days-of-aws-using-terraform-day-21-8eb27ee0c283)
* [A Guide to HashiCorp Certified Terraform Associate](https://sanoojm.medium.com/a-giude-to-hashicorp-certified-terraform-associate-cd9b21699139)


Registry

* https://registry.terraform.io/
* https://github.com/cloudposse
* https://gruntwork.io/infrastructure-as-code-library/
* https://github.com/devops-workflow
* https://github.com/clouddrove


Articles

* [Blog series from gruntworks](https://blog.gruntwork.io/a-comprehensive-guide-to-terraform-b3d32832baca)
* [How to Build Reusable, Composable, Battle tested Terraform Modules](https://www.youtube.com/watch?v=LVgP63BkhKQ)
* [Terraform Up and Running](https://blog.gruntwork.io/terraform-up-running-2nd-edition-early-release-is-now-available-b104fc29783f)
* [Introduction to HashiCorp Terraform with Armon Dadgar](https://www.youtube.com/watch?v=h970ZBgKINg)
* [Analysis of the software architecture of Terraform's core](https://uvicdsa.gitbook.io/uvicdsa19/chapter-9)
* [Terraform code auto-generation from cloudcraft](https://medium.com/faun/modules-tf-convert-visual-aws-diagram-into-terraform-configurations-e61fb0574b10)
* [Recovering Terraform State](https://medium.com/@abtreece/recovering-terraform-state-69c9966db71e)
* [Zero Downtime updates with terraform](https://www.hashicorp.com/blog/zero-downtime-updates-with-terraform)
* [using kubernetics/docker provider](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs)