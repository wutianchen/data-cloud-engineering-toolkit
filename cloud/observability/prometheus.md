# Prometheus

## Feature

* prometheus PodMonitor and ServiceMonitor
* prometheus remote wrtie


## (Re)Labelling

* [How relabeling in Prometheus works](https://grafana.com/blog/2022/03/21/how-relabeling-in-prometheus-works/)
* https://prometheus.io/docs/prometheus/latest/configuration/configuration/#relabel_config



## Reference

* [My Philosophy on Alerting](https://docs.google.com/document/d/199PqyG3UsyXlwieHaqbGiWVa8eMWi8zzAn0YfcApr8Q/edit#)
* [Chris Siebenmann's Blog](https://utcc.utoronto.ca/~cks/space/blog/)
* [Prometheus Best Practice and Beastly Pitfalls](https://promcon.io/2017-munich/slides/best-practices-and-beastly-pitfalls.pdf)

* Prometheus - Monitoring system & time series database : https://prometheus.io/
* alertmanager: https://github.com/prometheus/alertmanager
* grafana: https://grafana.com/
* https://alerta.io/ 
* https://copyconstruct.medium.com/logs-and-metrics-6d34d3026e38
* https://copyconstruct.medium.com/monitoring-in-the-time-of-cloud-native-c87c7a5bfa3e
* https://copyconstruct.medium.com/monitoring-and-observability-8417d1952e1c
* Reliable Insights – Robust Perception | Prometheus Monitoring Experts: https://www.robustperception.io/blog/