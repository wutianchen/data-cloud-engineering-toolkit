# Observability

There are three objects in observability metrics, logs and traces


* [openTelemetry](https://opentelemetry.io/): OpenTelemetry is a collection of APIs, SDKs, and tools. Use it to instrument, generate, collect, and export telemetry data (metrics, logs, and traces) to help you analyze your software’s performance and behavior.


## Components

tbd


## Federatable tsdb with prometheus

* [Levitate](https://last9.io/levitate-tsdb/)
* [Thanos](https://thanos.io/): Open source, highly available Prometheus setup with long term storage capabilities
* [Cortex](https://cortexmetrics.io/) : Horizontally scalable, highly available, multi-tenant, long term storage for Prometheus


## Reference

* [My Philosophy on Alerting](https://docs.google.com/document/d/199PqyG3UsyXlwieHaqbGiWVa8eMWi8zzAn0YfcApr8Q/edit#)
* [Chris Siebenmann's Blog](https://utcc.utoronto.ca/~cks/space/blog/)
* [Prometheus Best Practice and Beastly Pitfalls](https://promcon.io/2017-munich/slides/best-practices-and-beastly-pitfalls.pdf)
* [What is Prometheus Remote Write](https://last9.io/blog/what-is-prometheus-remote-write/)

* Prometheus - Monitoring system & time series database : https://prometheus.io/
* alertmanager: https://github.com/prometheus/alertmanager
* grafana: https://grafana.com/
* https://alerta.io/ 
* https://copyconstruct.medium.com/logs-and-metrics-6d34d3026e38
* https://copyconstruct.medium.com/monitoring-in-the-time-of-cloud-native-c87c7a5bfa3e
* https://copyconstruct.medium.com/monitoring-and-observability-8417d1952e1c
* Reliable Insights – Robust Perception | Prometheus Monitoring Experts: https://www.robustperception.io/blog/