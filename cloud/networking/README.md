# Networking

* [about terraform cidrsubnet function and subnet address space](https://ntwobike.medium.com/how-cidrsubnet-works-in-terraform-f6ccd8e1838f)
* [AWS Networking (VPCs, Subnets, CIDR) and Terraform Integration](https://kevonmayers31.medium.com/aws-networking-vpcs-subnets-cidr-and-terraform-integration-97faf8710746)