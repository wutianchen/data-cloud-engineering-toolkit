# Operator

operator frameworks

* Go
* Ansible
* Helm

development toolkit (operator-sdk) https://sdk.operatorframework.io/

operator repository: https://operatorhub.io/


## Operator Framework

* [operator sdk (java)](https://sdk.operatorframework.io/)
* [kops: python k8s operator](https://kops.sigs.k8s.io/)

## Reference

* https://www.youtube.com/watch?v=VAojjIYVhGk