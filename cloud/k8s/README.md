# K8s

k8s is a container orchastration tool

* Architecture
* Distributions
* Containization


## Alternative Container Orchastration Tool

* docker swarm
* apache mesos/Marathon
* cloud foundry
* amazon ecs
* hashicorp nomad
* [portainer](https://www.portainer.io/)


## Tools, Modules and Products


#### Tools

| Tool   |      Link      |  Description |
|----------|:-------------:|------:|
| ksniff |  https://github.com/eldadru/ksniff | A kubectl plugin that utilize tcpdump and Wireshark to start a remote capture on any pod in your Kubernetes cluster. |
| stern |    https://github.com/stern/stern  |  Stern allows you to tail multiple pods on Kubernetes and multiple containers within the pod. Each result is color coded for quicker debugging |
| kubewatch | https://github.com/robusta-dev/kubewatch?ref=kodekloud.com |    kubewatch is a Kubernetes watcher that publishes notification to available collaboration hubs/notification channels. Run it in your k8s cluster, and you will get event notifications through webhooks. |
| kubernetes-event-exporter |   https://github.com/resmoio/kubernetes-event-exporter/tree/master  |  This tool allows exporting the often missed Kubernetes events to various outputs so that they can be used for observability or alerting purposes |
| kspan |   https://github.com/weaveworks-experiments/kspan  |  Most Kubernetes components produce Events when something interesting happens. This program turns those Events into OpenTelemetry Spans, joining them up by causality and grouping them together into Traces |
| kube-events |   https://github.com/kubesphere/kube-events  |  Kube-events revolves around Kubernetes Event, covering multi-dimensional processing of them, such as emitting events to sinks, issuing notifications and generating alerts. In some of these dimensions, configurable filtering rules are provided to meet different business needs |
| Quarkus |    https://quarkus.io/  | A JVM tool to develope k8s native application |
| kluctl |  https://kluctl.io/  | providing extra features to manage k8s resources, additionally to kubectl. An introduction vide: https://www.youtube.com/watch?v=9LoYLjDjOdg |
| ArgoCD | https://argo-cd.readthedocs.io/en/stable/ | cd tool specifically for k8s deployment  |
| kubeadmin | https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/ | create a minimum viable Kubernetes cluster that conforms to best practices  |
| crossplane |  https://www.crossplane.io/  | unify public cloud services onto k8s cluster |
| kubecost |  https://github.com/kubecost  | Kubecost is a monitoring application which provides real-time cost visibility and insights for teams using Kubernetes, helping you continuously reduce your cloud costs |
| opencost |  https://www.opencost.io/  | OpenCost is a vendor-neutral open source project for measuring and allocating cloud infrastructure and container costs in real time |
| kubicorn |  http://kubicorn.io/  | Create, manage, snapshot, and scale Kubernetes infrastructure in the public cloud |


* [How to collect Kubernetes events](https://isitobservable.io/observability/kubernetes/how-to-collect-kubernetes-events)
* [eventrouter](https://github.com/vmware-archive/eventrouter?ref=kodekloud.com)


#### Modules

| Module   |      Link      |  Description |
|----------|:-------------:|------:|
| external-dns | https://github.com/kubernetes-sigs/external-dns | ExternalDNS synchronizes exposed Kubernetes Services and Ingresses with DNS providers |
| kubedb | https://kubedb.com/ | Run Production-Grade Databases on Kubernetes |
| cloudnative-cnpg | https://cloudnative-pg.io/ | Run PostgreSQL. The Kubernetes way |

* flink operator: https://nightlies.apache.org/flink/flink-kubernetes-operator-docs-main/
* strimzi operator: https://strimzi.io/


#### Products & Framework

| Product   |      Link      |  Description |
|----------|:-------------:|------:|
| volcano |  https://volcano.sh/en/#required | Volcano is system for running high-performance workloads on Kubernetes |
| knative |  https://knative.dev/docs/install/ | Serverless framework on k8s |
| helmfile |  https://github.com/helmfile/helmfile | helm chart at scale |
| kustomize |  https://github.com/kubernetes-sigs/kustomize |  customize raw, template-free YAML files for multiple purposes https://www.youtube.com/watch?v=spCdNeNCuFU |
| kubebuilder |  https://github.com/kubernetes-sigs/kubebuilder | Kubebuilder is a framework for building Kubernetes APIs using custom resource definitions (CRDs). |

Autoscaling:

There are two types of autoscaling 1) cluster node autoscaling 2) application autoscaling

| Product   |      Link      |  Description |
|----------|:-------------:|------:|
| KEDA |  https://keda.sh/ | Kubernetes Appication Event-driven Autoscaling |
| Karpenter | https://karpenter.sh/ | Cluster node autoscaler |
| Cluster Autoscaler | https://github.com/kubernetes/autoscaler| autoscaling-related components for Kubernetes |


#### Others

* [Kubernetes SIGs](https://github.com/kubernetes-sigs)
* [Kubersphere](https://github.com/kubesphere)
* [6 Serverless Frameworks on Kubernetes You Need to Know](https://www.appvia.io/blog/serverless-on-kubernetes)
* https://www.youtube.com/watch?v=79C05LcfulY
* [Open Container Initiative](https://opencontainers.org/)
* [Kubernetes Deployment Antipattern](https://codefresh.io/blog/kubernetes-antipatterns-1/)


#### Talks

* [AWS re:Invent 2023 - Platform engineering with Amazon EKS (CON311)](https://www.youtube.com/watch?v=eLxBnGoBltc)


## Famous Images

* pause
* busybox