## Distribution - k8s

* minicube
* microk8s
* k3s
* Kind

comparison of different distributions

* https://microk8s.io/compare



minikube dashboard: https://shubham-singh98.medium.com/minikube-dashboard-in-aws-ec2-881143a2209e


## microk8s

#### installation on macos

How to Setup a Development Environment on local machine (mac)

https://microk8s.io/docs/install-macos

```bash
brew install ubuntu/microk8s/microk8s
microk8s install
```

#### common commands

* Check the status while Kubernetes starts

```bash
microk8s status --wait-ready
```


* start kubernetes cluster

```bash
microk8s start
```

* destroy kubernetes cluster

```bash
microk8s stop
```

* using kubectl command tool

```bash
microk8s kubectl get all --all-namespaces
```

* Turn on the services you want
microk8s enable dashboard dns registry istio
Try microk8s enable --help for a list of available services built in. The microk8s disable command turns off a service.
