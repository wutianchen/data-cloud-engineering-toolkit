## What is Container

The shared context of a Pod is a set of Linux namespaces, cgroups, and potentially other facets of isolation - the same things that isolate a container. Within a Pod's context, the individual applications may have further sub-isolations applied



different distribution of container technologies

* docker
* containerd
* CRI-O
* kata


## different types of containers in k8s context

* init container: https://kubernetes.io/docs/concepts/workloads/pods/init-containers/
* ephemeral container: https://kubernetes.io/docs/concepts/workloads/pods/ephemeral-containers/
* app container
* shpod: https://github.com/jpetazzo/shpod


