# Docker

* [Understanding How the Docker Daemon and Docker CLI Work Together](https://nickjanetakis.com/blog/understanding-how-the-docker-daemon-and-docker-cli-work-together)
* [Docker RUN vs CMD vs ENTRYPOINT](https://codewithyury.com/docker-run-vs-cmd-vs-entrypoint/)
* [Demystifying ENTRYPOINT and CMD in Docker](https://aws.amazon.com/blogs/opensource/demystifying-entrypoint-cmd-docker/)
* [Docker ARG, ENV and .env - a Complete Guide](https://vsupalov.com/docker-arg-env-variable-guide/#arg-and-env-availability)
* [What is the difference between ports and expose in docker-compose?](https://stackoverflow.com/questions/40801772/what-is-the-difference-between-ports-and-expose-in-docker-compose)
* [Connection refused? Docker networking and how it impacts your image](https://pythonspeed.com/articles/docker-connection-refused/)


Docker create two bridges that corrupts my internet access: https://stackoverflow.com/questions/43988006/docker-create-two-bridges-that-corrupts-my-internet-access



## Commands

* remove all stopped containers

```bash
docker rm $(docker ps -a -q)
```

* remove all untagged images (https://jimhoskins.com/2013/07/27/remove-untagged-docker-images.html)

```bash
docker rmi $(docker images | grep "^<none>" | awk "{print $3}")
```