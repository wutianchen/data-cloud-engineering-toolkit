# Container Technology

* [docker](https://www.docker.com/)
* [containerd](https://containerd.io/)
* Podman
* Buildah
* CRI-O (redhat)


## Tools

* chart testing: https://github.com/helm/chart-testing

ct is the the tool for testing Helm charts. It is meant to be used for linting and testing pull requests. It automatically detects charts changed against the target branch. (github action https://github.com/helm/chart-testing-action)

https://faun.pub/automate-your-helm-chart-testing-workflow-with-github-actions-b702f0f820f6

## Build Image

### The Docker Way

* [Docker in Docker?](https://itnext.io/docker-in-docker-521958d34efd)
* [Docker on Docker](https://www.youtube.com/watch?v=SGhKRKsfgQY)
* [Docker-in-Docker vs Docker-out-of-Docker](http://tdongsi.github.io/blog/2017/04/23/docker-out-of-docker/)
* [Docker can now run within Docker](https://www.docker.com/blog/docker-can-now-run-within-docker/)
* [~jpetazzo/Using Docker-in-Docker for your CI or testing environment? Think twice](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/)
* [Gitlab CI: Build & push Docker image to AWS ECR (Elastic Container Registry)](https://www.youtube.com/watch?v=jg9sUceyGaQ)
* [Builder pattern vs. Multi-stage builds in Docker](https://blog.alexellis.io/mutli-stage-docker-builds/)

### The Kaniko Way

* https://github.com/GoogleContainerTools/kaniko
* [Building containers without Docker](https://blog.alexellis.io/building-containers-without-docker/)
* [Kaniko - Building Container Images In Kubernetes Without Docker](https://www.youtube.com/watch?v=EgwVQN6GNJg)
* [Kaniko Tutorial : Build container images without Docker Daemon](https://karthi-net.medium.com/kaniko-tutorial-build-container-images-without-docker-daemon-383ca0312937)
* [Hands-on Introduction to Kaniko | Rawkode Live](https://www.youtube.com/watch?v=7J_j42jddDg)

### The Packer Way

https://www.packer.io/
