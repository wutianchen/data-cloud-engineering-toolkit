# Probes

* readiness Probe
* liveness Probe
* Startup Probe


## Readiness Probe (Healthcheck ?)

Readiness probes determine when a container is ready to start accepting traffic.

If the readiness probe returns a failed state, Kubernetes removes the pod from all matching service endpoints.


## Liveness Probe

Liveness probes determine when to restart a container. For example, liveness probes could catch a deadlock when an application is running but unable to make progress.

Liveness probes do not wait for readiness probes to succeed.


## Startup Probe

If such a probe is configured, it disables liveness and readiness checks until it succeeds.



## Reference

* https://kubernetes.io/docs/concepts/configuration/liveness-readiness-startup-probes/
* https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/
* [Kubernetes Health Checks: Liveness vs. Readiness vs. Startup Probe](https://www.youtube.com/watch?v=fqfieWP1jY4)