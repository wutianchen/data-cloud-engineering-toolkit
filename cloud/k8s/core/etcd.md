# ectd

https://etcd.io/



## Reference

* [Understanding the Workflow: How Kubernetes Uses ETCD to Store and Manage Cluster Information](https://medium.com/@bala.praveenkumar/understanding-the-workflow-how-kubernetes-uses-etcd-to-store-and-manage-cluster-information-2196355a5422#:~:text=Using%20etcdctl%20to%20Query%20ETCD,current%20state%20of%20the%20cluster.)
* [Deep Dive: etcd - Jingyi Hu, Google](https://www.youtube.com/watch?v=DrtdrdwDpZE)
* [Secrets of Running Etcd - Marek Siarkowicz, Google](https://www.youtube.com/watch?v=aJVMWcVZOPQ)