# Kubectl

two important aspects of kubectl actions (command) and resources https://kubernetes.io/docs/reference/kubectl/


default location of a configuration file **.kube/config**


several ways to get help

1. help

```bash
kubectl help
```

2. explain

```bash
$ kubectl explain --recursive. 
$ kubectl explain node
$ kubectl explain node.spec
```

3. API documentation



## Resrouces (Kind)

list all resources

```bash
kubectl api-resources
```


* Node
* Pod
* CronJob
* Deployment
* ReplicaSet
* DeamonSet
* Service (ClusterIp, NodePort, Load Balancer, ExternalIP, ExternalName)
* Ingress
* EndpointSlice
* Endpoint


## Command

* get
* describe
* explain
* run 
* create
* expose

other commands

* rollout
* set
* edit


