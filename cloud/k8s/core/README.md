# Architecture

## Components

control plane

* kube-apiserver
* etcd (also a CNCF project, https://etcd.io/)
* kube-scheduler
* kube-controller-manager (controllers) (cloud-controller-manager)
* Kubelet
* kube-proxy (kube-router)
* CoreDNS
* Addons (belong within the kube-system namespace, list of addons https://kubernetes.io/docs/concepts/cluster-administration/addons/)



learning about k8s internals

* https://www.youtube.com/watch?v=YIBQrP1grPE
* https://www.youtube.com/watch?v=F7-ZxWwf4sY
* https://www.youtube.com/watch?v=J0n4n2RtbGs
* https://www.youtube.com/watch?v=7wdUa4Ulwxg&t=84s




## Addons

* tools for monitoring resources (https://kubernetes.io/docs/tasks/debug/debug-cluster/resource-usage-monitoring/)

The resource metrics pipeline provides a limited set of metrics related to cluster components such as the Horizontal Pod Autoscaler controller, as well as the `kubectl top` utility. These metrics are collected by the lightweight, short-term, in-memory metrics-server and are exposed via the metrics.k8s.io API.

* metric server: https://github.com/kubernetes-sigs/metrics-server


* calico (open source networking and network security solution for caintainers): https://projectcalico.docs.tigera.io/about/about-calico
* CoreDNS


## Others

there are two different event-based handling edge and level-trigger semantics


* [Into the Core of Kubernetes: The Internals of etcd](https://www.youtube.com/watch?v=YIBQrP1grPE)