# Minikube


## Addons

minikube has packaged common use case into addons

```bash
minikube addons list
```

#### metrics-server

```bash
minikube addons enable metrics-server
```

> **_NOTE:_** how can we implement a metrics-server without using minikube ?



#### ingress

```bash
minikube addons enable ingress
```


#### dashboard

```bash
minikube addons enable dashboard
```

open dashboard using 

```bash
minikube dashboard
```