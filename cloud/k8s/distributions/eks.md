# EKS

mapping iam principals to k8s RBAC with configmap aws-auth

```bash
kubectl -n kube-system describe aws-auth
´``

## Provision

#### Blueprints

* https://github.com/aws-ia/terraform-aws-eks-blueprints
* https://github.com/aws-ia/terraform-aws-eks-blueprints-addons/tree/main
* https://github.com/aws-ia/terraform-aws-eks-blueprints-addon ?





#### Others

* [cloudposse eks module](https://github.com/cloudposse/terraform-aws-eks-cluster)
* [fundamental eks cluster module](https://github.com/terraform-aws-modules/terraform-aws-eks)


## Advanced Features

* [Amazon EKS Pod Identity: a new way for applications on EKS to obtain IAM credentials](https://aws.amazon.com/blogs/containers/amazon-eks-pod-identity-a-new-way-for-applications-on-eks-to-obtain-iam-credentials/)