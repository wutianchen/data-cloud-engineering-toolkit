# Distribution

## Kubernetes distributions

* kubernetes "vanilla upstream"
* cloudmanaged distros: AKS, GKE, EKS, DOK
* self-managed distros: Redhat Openshift, Docker Enterprise, Rancher etc


## Non-Production (Dev/CI testing) Distribution

* microk8s: https://microk8s.io/
* Docker Desktop


certified distribution


* [Nubenetes](https://nubenetes.com/)
* [ArtifactHUB](https://artifacthub.io/)
* [minikube](https://minikube.sigs.k8s.io/docs/start/)
* [kind](https://kind.sigs.k8s.io/)
* [k3d](https://k3d.io/v5.4.9/)
* [k3s](https://k3s.io/)
* [kubesphere](https://kubesphere.io/)


A very good list can be found here https://helm.sh/docs/topics/kubernetes_distros/