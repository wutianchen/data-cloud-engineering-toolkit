# Deployment Strategies

| Deployment Strategy              | Feature | Description |
| :---------------- | :------: | ----: |
| Blue Green        |   Keep the older version undestroyed for quick fallback and swtich   |  |
| Canary           |   Rollout new release to a small subset of users first for real-world testing   |  |



## Blue Green Deployment

tbd

## Canary Deployment

https://www.youtube.com/watch?v=rYOGz3n3Gw4

## Rolling Upgrade

tbd

## Recreate Deployment

tbd

## Shadow Deployment

tbd

## A/B Testing Deployment

tbd

## Incremental Deployment

tbd

## Big Bang Deployment

tbd


* blue-green deployment (continuous deployment model): https://martinfowler.com/bliki/BlueGreenDeployment.html
* canary deployment: https://martinfowler.com/bliki/CanaryRelease.html