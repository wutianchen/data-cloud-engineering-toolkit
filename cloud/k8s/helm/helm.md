# Helm

## Tools

| Tool   |      Description      |  Reference |
|----------|:-------------:|------:|
| helm unittest |  Unit test for helm chart in YAML to keep your chart consistent and robust | https://github.com/helm-unittest/helm-unittest |
| chart testing |  lint etc   | https://github.com/helm/chart-testing/tree/main |
| chart releasing tool |  release   | https://github.com/helm/chart-releaser |

    
* helm lint
* helm test

* [Linting and Testing Helm Charts](https://redhat-cop.github.io/ci/linting-testing-helm-charts.html)

## Reference


* [A deep dive into Helm Dependencies](https://anaisurl.com/a-deep-dive-into-helm-sub-charts/)
* [Simplifying Helm Chart Management: A Practical Guide to Creating Wrappers for External Dependencies](https://medium.com/@erikstrm_11611/simplifying-helm-chart-management-a-practical-guide-to-creating-wrappers-for-external-dependencies-722436050c6e)
* [A Helm chart trick for dependency highlighting](https://sitaram.substack.com/p/a-helm-chart-trick-for-dependency)