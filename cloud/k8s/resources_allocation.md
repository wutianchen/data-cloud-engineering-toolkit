# Resources Allocation

drain

```bash
kubectl drain
```

taint

```bash
kubectl taint
```

cordoned



## Limits and Requests

limit is the upper boundary <= , requests is the lower boundary >= . https://sysdig.com/blog/kubernetes-limits-requests/


tbd

Node Local Storage Evictions

```yaml
resources:
  requests:
    ephemeral-storage: "1Gi"
  limits:
    ephemeral-storage: "2Gi"
```

## PodDisruptionBudget (PDB)

apiVersion: policy/v1
link: https://kubernetes.io/docs/tasks/run-application/configure-pdb/

* [Elevating your Kubernetes Cluster’s Resilience: Pod Disruption Budget](https://medium.com/@bregman.arie/elevating-your-kubernetes-clusters-resilience-pod-disruption-budget-43979a1cf104)
* [Pod Disruption Budget: The Practical Guide](https://medium.com/@bregman.arie/pod-disruption-budget-the-practical-guide-1eb4ff78b6c9)

## PriorityClass

tbd

## Node affinity and Anti-Affinity

tbd

## Taints and Toleration

tbd

## Node Anti-Eviction Configuration

configuration on the kublet

tbd

## Static pods

tbd