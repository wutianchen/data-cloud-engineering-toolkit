# Architecture

https://aws.amazon.com/blogs/architecture/

five pillars

* Performance
* Operational Excellence
* Cost Optimization
* Reliability
* Security

AWS Well Architected Framework: https://d1.awsstatic.com/whitepapers/architecture/AWS_Well-Architected_Framework.pdf



## Accounts Architecture

* [Deciding between large accounts or micro accounts](https://aws.amazon.com/blogs/mt/deciding-between-large-accounts-or-micro-accounts-for-distributed-operations-at-aws/)
* [Organizing Your AWS Environment Using Multiple Accounts AWS Whitepaper](https://docs.aws.amazon.com/pdfs/whitepapers/latest/organizing-your-aws-environment/organizing-your-aws-environment.pdf)


landing zone. what is a landing zone https://docs.aws.amazon.com/prescriptive-guidance/latest/migration-aws-environment/understanding-landing-zones.html



## Bastion-less with AWS SSM

https://www.1cloudhub.com/connect-ec2-private-instance-using-session-manager-go-bastion-less/

Advantages:

* Supports Linux / Windows and public or private instances.
* No need to open the ports in the security groups
* No bastion host required to connect to the private hosts
* No need for SSH keys or passwords to connects the instances
* Manage and record the entire sessions and store the recordings in Cloudwatch and S3
* One click accesses to the instances from console.
* Managed centralised access control to the instances using IAM policies.

Limitation:

The limitation in using AWS Session Manager instead of SSH is that it doesn’t  allow files to be transferred. The workaround to address this issue is to use s3 bucket and configure AWSCLI to exchange data
