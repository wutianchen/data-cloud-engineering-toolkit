# Cost

## VPC

#### 1. public ip address 

As you may know, IPv4 addresses are an increasingly scarce resource and the cost to acquire a single public IPv4 address has risen more than 300% over the past 5 years (https://aws.amazon.com/blogs/aws/new-aws-public-ipv4-address-charge-public-ip-insights/)