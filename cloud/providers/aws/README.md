# Amazon Web Service

* [IAM Policy Simulator](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_testing-policies.html)


## Libraries

* awssome lambda layers: https://github.com/mthenw/awesome-layers

* s3fs: S3FS builds on botocore to provide a convenient Python filesystem interface for S3 https://s3fs.readthedocs.io/en/latest/
* fs-s3fs: S3FS is a PyFilesystem interface to Amazon S3 cloud storage  https://pypi.org/project/fs-s3fs/
* s3cmd: S3cmd : Command Line S3 Client and Backup for Linux and Mac https://s3tools.org/s3cmd
* minio: https://min.io/
* aws-vault: a tool to securely store and access AWS credentials in a development environment  https://github.com/99designs/aws-vault
* localstack: A fully functional local AWS cloud stack https://github.com/localstack/localstack




## Metadata

```bash
curl http://169.254.169.254/latest/meta-data/iam/info
```

https://ops.tips/blog/blocking-docker-containers-from-ec2-metadata/ 

Metadata endpoint / ECS Task Metadata

http://169.254.169.254


## Knowledge Points

* Signature Version 4 Signing Process: https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_aws-signing.html
* A Comprehensive Guide to Authenticating to AWS on the Command Line: https://blog.gruntwork.io/a-comprehensive-guide-to-authenticating-to-aws-on-the-command-line-63656a686799