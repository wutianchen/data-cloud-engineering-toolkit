# mypy

* [mypy - Optional Static Typing for Python](https://mypy-lang.org/)
* [PEP 484 – Type Hints | peps.python.org ](https://peps.python.org/pep-0484/)
* [PEP 526 – Syntax for Variable Annotations | peps.python.org ](https://peps.python.org/pep-0526/)
* [Applying mypy to real world projects](https://calpaterson.com/mypy-hints.html)
* [Python Type Checking (Guide) – Real Python](https://realpython.com/python-type-checking/)

Add type hints or stubs to their code, and include a file named py.typed within the package they distribute to PyPi (or any other package repository). The presence of this marker makes the package PEP-561-aware. The mypy docs also have more info about PEP-561-aware packages.

* [Carl Meyer - Type-checked Python in the real world - PyCon 2018 ](https://www.youtube.com/watch?v=pMgmKJyWKn8)
* [Greg Price - Clearer Code at Scale: Static Types at Zulip and Dropbox - PyCon 2018](https://www.youtube.com/watch?v=0c46YHS3RY8)
* [Bernat Gabor - Type hinting (and mypy) - PyCon 2019](https://www.youtube.com/watch?v=hTrjTAPnA_k)
* [Convincing an entire engineering org to use and like mypy](https://www.youtube.com/watch?v=S75VVAZmqZ8)
* [Stephan Jaensch - Type annotations with larger codebases](https://www.youtube.com/watch?v=pWnETtscNS0)
* [Pieter Hooimeijer - Types, Deeper Static Analysis, and you - PyCon 2018](https://www.youtube.com/watch?v=hWV8t494N88)
* [typing-json](https://pypi.org/project/typing-json/)
* [Type checking with JSON Schema in Python](https://medium.com/@erick.peirson/type-checking-with-json-schema-in-python-3244f3917329)
