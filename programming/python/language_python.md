# Python DevOps

Python Code Quality Authority: https://github.com/PyCQA


## Package Management

* pipx: a python cli tool installer https://pypa.github.io/pipx/comparisons/
* pip:
* pipenv: https://realpython.com/pipenv-guide/
* pip-tools: https://pypi.org/project/pip-tools/
* poetry: https://medium.com/@paweldudzinski/python-applications-continuous-integration-with-poetry-and-gitlab-pipelines-ac539888251a


## Static Code Analysis and formatting

what is the difference between static code analysis and formatting


* black
* flake8
* pylint
* autoflake
* isort



#### PEP

https://peps.python.org/pep-0020/

```python
import this
```


## Testing

* pytest