# Python

* [Python Code Quality Authority](https://github.com/PyCQA)
* [PEP8](https://peps.python.org/pep-0008/)




## ToLearn

* article: https://towardsdatascience.com/how-to-make-the-most-of-pydantic-aa374d5c12d
* ArjanCodes: https://www.youtube.com/watch?v=Vj-iU-8_xLs
* Erik Rose Constructive Code Review PyCon 2017: https://www.youtube.com/watch?v=iNG1a--SIlk
* [Carol Willing - Practical Sphinx - PyCon 2018](https://www.youtube.com/watch?v=0ROZRNZkPS8)
* [Bryson Tyrrell - Your code should document itself! Embedding documentation into your Python projects](https://www.youtube.com/watch?v=JQ8RQru-Y9Y)
* [Building Docs like Code: Continuous Integration for Documentation](https://www.youtube.com/watch?v=ftnVllssoI8)


## Libraries


| Package Name      | Description |  Component |
| ----------- | ----------- | ---------------- |
| pathlib      | https://docs.python.org/3/library/pathlib.html#module-pathlib       | manipulate path |
| loguru      | https://github.com/Delgan/loguru       | logging |
| environ     | https://pypi.org/project/python-environ/        | environment variable |
| pytest     | https://docs.pytest.org/en/7.2.x/        | testing |
| aws-embedded-metrics-python     | https://github.com/awslabs/aws-embedded-metrics-python    | aws specific programming, cloudwatch. logging |
| aws lambda powertools for python    | https://awslabs.github.io/aws-lambda-powertools-python/2.9.0/    | aws specific programming, lambda |
| awswrangler    |     | pandas on aws |
| fastjsonschema    |  https://horejsek.github.io/python-fastjsonschema/   | json schema validator (the fast one) |
| pyhocon    |  https://github.com/chimpler/pyhocon/tree/master | python for hocon |
| environ     | https://pypi.org/project/python-environ/        | environment variable |
| pydantic    | https://docs.pydantic.dev/    | data validation and setting management, a package for python dataclasses https://docs.python.org/3/library/dataclasses.html with extra features |
| sqlalchemy    |  https://www.sqlalchemy.org/   | database connector |
| pyparsing    |   https://pyparsing-docs.readthedocs.io/en/latest/index.html  | implementation of [parsing expression grammar](https://en.wikipedia.org/wiki/Parsing_expression_grammar) in python |
| moto    |  https://pypi.org/project/moto/   | test suite for aws cloud (cloud resource mocking) |
| testcontainers-python    |  https://testcontainers-python.readthedocs.io/en/latest/README.html#testcontainers-python , https://github.com/testcontainers/testcontainers-python  | testcontainers-python facilitates the use of Docker containers for functional and integration testing (including PostgresDB, Kafka etc) |
| hypothesis    |  https://hypothesis.readthedocs.io/en/latest/   | library for python testing |
| dacite    |  https://github.com/konradhalas/dacite   | simplify creation of data classes from dictionaries |
| dictwrapper    |  https://pypi.org/project/dictwrapper/   | this python library implements dictionary-like objects |
| pytest-lazy-fixture    |  https://pypi.org/project/pytest-lazy-fixture/   | lazy evaluation of the pytest fixture |
| tenacity    |  https://tenacity.readthedocs.io/en/latest/   | for retrying decorator etc |


* inflection: https://pypi.org/project/inflection/
* jinja: https://geoffruddock.com/sql-jinja-templating/

Four Types of Parameters and Two Types of Arguments in Python: https://towardsdatascience.com/four-types-of-parameters-and-two-types-of-arguments-in-python-357ccfdea3db


## Framework

* [streamlit](https://github.com/streamlit/streamlit)
* [serverless programming framework on aws - lambda]()

#### Internal Features

* collections
