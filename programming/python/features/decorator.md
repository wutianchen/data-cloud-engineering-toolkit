# Decorator

fundamental articles:

* [Primer on Python Decorators](https://realpython.com/primer-on-python-decorators/#more-real-world-examples)
* [awesome python decorator](https://github.com/lord63/awesome-python-decorator)


informational articles

* [12 Python Decorators to Take Your Code to the Next Level](https://towardsdatascience.com/12-python-decorators-to-take-your-code-to-the-next-level-a910a1ab3e99)
* [9 Python Built-In Decorators That Optimize Your Code Significantly](https://medium.com/techtofreedom/9-python-built-in-decorators-that-optimize-your-code-significantly-bc3f661e9017)

