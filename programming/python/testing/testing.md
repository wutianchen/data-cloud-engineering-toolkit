# Testing


## Chao Engineering

* [Chaos Mesh](https://chaos-mesh.org/)
* [ChaosToolkit](https://chaostoolkit.org/)


## pytest

plugins:

* pytest-docker: https://pypi.org/project/pytest-docker/
* pytest-mock: https://pypi.org/project/pytest-mock/
* moto: http://docs.getmoto.org/en/latest/docs/getting_started.html



* pytest-order https://pypi.org/project/pytest-order/

# Articles

how to get directory with test from fixture in conftest.py https://medium.com/opsops/how-to-get-directory-with-test-from-fixture-in-conftest-py-275b566fcc00