# Python Testing

unittest
pytest



## Mocking and Patching

| Way| Description    | Link    |
| :---:   | :---: | :---: |
| monkeypatch |    |    |
| patch in unittest |    |    |
| unittest - mock |    |  https://docs.python.org/3/library/unittest.mock.html |
| pytes - monkeypatch|    | https://docs.pytest.org/en/7.1.x/how-to/monkeypatch.html#monkeypatching   |
| moto |  aws resource mocking  |  https://docs.getmoto.org/en/latest/docs/getting_started.html  |
| pytest-mock -mocker | unittest mocker is directly accessible within mocker  |  https://pypi.org/project/pytest-mock/  |
| testscenarios | testscenarios provides clean dependency injection for python unittest style tests (https://docs.pytest.org/en/stable/example/parametrize.html#a-quick-port-of-testscenarios)  | https://pypi.org/project/testscenarios/  |

library: hypothesis





topics:

* mocking function: https://www.freblogg.com/pytest-functions-mocking-1
* mocking and patching: https://pytest-with-eric.com/mocking/mocking-vs-patching/


## Debugging

how to debug in pytest https://seleniumbase.com/the-ultimate-pytest-debugging-guide-2021/


Method1:

```bash
pytest --pdb
```


## Interesting articles


* [Pytest with Eric](https://pytest-with-eric.com/)
* [Mock Environment Variable](https://adamj.eu/tech/2020/10/13/how-to-mock-environment-variables-with-pytest/)
* Pytest with Marking, Mocking, and Fixtures in 10 Minutes: https://towardsdatascience.com/pytest-with-marking-mocking-and-fixtures-in-10-minutes-678d7ccd2f70
* Understanding the Python Mock Object Library: https://realpython.com/python-mock-library/#the-mock-object

