# Sphinx

style

* autodoc, sphinx style
* [Numpy and Google docstring style](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)


* [Read the Docs](https://readthedocs.org/)
* [Write the Docs](https://www.writethedocs.org/)
* [Theme](https://sphinx-themes.org/)

restructuredText

* [Python docstring reStructuredText style](https://gist.github.com/jesuGMZ/d83b5e9de7ccc16f71c02adf7d2f3f44)
* [reStructuredText Primer](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html)
* [How can I generate documentation for a Python property setter using Sphinx?](https://stackoverflow.com/questions/17478040/how-can-i-generate-documentation-for-a-python-property-setter-using-sphinx)
