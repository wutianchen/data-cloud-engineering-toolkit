# AWS Lambda

| Package Name      | Description |  Component |
| ----------- | ----------- | ---------------- |
| lambda-cache    | https://pypi.org/project/lambda-cache/    | simple caching for aws lambda |
| aws-embedded-metrics-python     | https://github.com/awslabs/aws-embedded-metrics-python    | aws specific programming, cloudwatch. logging |
| aws lambda powertools for python    | https://awslabs.github.io/aws-lambda-powertools-python/2.9.0/    | aws specific programming, lambda |


## Features

#### Lambda execution context

tbd

#### Lambda extension

* [lambda extension tutorial](https://www.youtube.com/watch?v=vEYffPn4Mtc&list=PLJo-rJlep0ECO8od7NRdfJ4OrnQ7TMAwj&index=1)
* [vector deployment using lambda extension](https://medium.com/@yury.sannikov/processing-aws-s3-logs-with-vector-on-aws-lambda-b2ba7dcd29e3)


## Reference

* [Understand the Lambda execution environment lifecycle](https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtime-environment.html)
* [Lambda Functions Caching mechanism](https://ruhulmus.medium.com/lambda-functions-caching-mechanism-807ee4a484c4#:~:text=lambda%2Dcache%20helps%20you%20cache,load%20on%20back%2Dend%20systems)