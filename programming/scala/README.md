# Scala

* [Scala Website](https://www.scala-lang.org/)
* [sbt](https://www.scala-sbt.org/)
* [Standard Library API](https://www.scala-lang.org/api/current/)
* [Scala Compiler: Dotty](https://github.com/lampepfl/dotty)
* [Scala programming guideline](https://netvl.github.io/scala-guidelines/code-style/naming.html)



## Try Interavtively

* https://scastie.scala-lang.org/
* scala worksheet

## VSC Extension

* metals https://marketplace.visualstudio.com/items?itemName=scalameta.metals


## Questions

* what is scalac (similar to javac) ? why can I compile code with scalac
* why there are no api version 3.2 for scala standard library ?


## Review Points

* what is the difference between val (so called `value`) and var (so called `variable`) ? https://docs.scala-lang.org/scala3/book/taste-vars-data-types.html
* what is type inference in scala and when to use implicit/explict type information ?
* what is the difference of foreach (for yield) and map function ?
* what is scala apply factory method ? how does it used on scala.collection data type (for example: List)
* what is type inference and semicolon inference in scala ? (what are the rules of these two inferences)
