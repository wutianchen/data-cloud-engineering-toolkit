## Tutorials and Documentations

* [Rock The JVM](https://rockthejvm.com/)
* [Scala & Functional Programming Essentials | Rock the JVM](https://www.udemy.com/course/rock-the-jvm-scala-for-beginners/?utm_source=adwords&utm_medium=udemyads&utm_campaign=DSA_Catchall_la.EN_cc.ROW&utm_content=deal4584&utm_term=_._ag_88010211481_._ad_535397282064_._kw__._de_c_._dm__._pl__._ti_dsa-406594358574_._li_9042604_._pd__._&matchtype=&gclid=CjwKCAjwoIqhBhAGEiwArXT7K8gE-Pi1vCSwc0uYeIaduroi2I92AeADNJRnH2449127Fx-qPIyYaBoCd_YQAvD_BwE)
* [Video series from Rock the JVM](https://www.youtube.com/watch?v=sby4rxdmabI&list=PLmtsMNDRU0BxryRX4wiwrTZ661xcp6VPM&index=3)
* [Learn Scala](https://docs.scala-lang.org/)
* [Scalacenter](https://scala.epfl.ch/)
* [Programming in Scala](https://people.cs.ksu.edu/~schmidt/705a/Scala/Programming-in-Scala.pdf)

Videos:

* [KEYNOTE Simply Scala Martin Odersky](https://www.youtube.com/watch?v=QRcD9Zc7eq4&t=21s)