# Software Design

* cohension and coupling: https://www.youtube.com/watch?v=eiDyK_ofPPM&list=PLC0nd42SBTaNuP4iB4L6SJlMaHE71FG6N&index=6
* programming towards interfaces (hiding details)

## Design Principles

concepts

* responsibility
* information
* type system, type information

#### SOLID Design Principles

tbd

#### GRASP Design Principles

General Responsibility Assignment Software Patterns

* [wikipedia GRASP (object-oriented design)](https://en.wikipedia.org/wiki/GRASP_(object-oriented_design))
* [GRASP Design Principles: Why They Matter (And How to Use Them)](https://www.youtube.com/watch?v=fGNF6wuD-fg&list=PLC0nd42SBTaNuP4iB4L6SJlMaHE71FG6N&index=2)


#### Micro-service Design Principle

Would user notice if you ran Chaos Monkey (https://netflix.github.io/chaosmonkey/), which terminates any container randomly ? If so, you may have more work making containers and applications more decoupled and transient.

* The twelve factor app: https://12factor.net/

## Design Patterns

| Pattern    | Problem to solve | Description |
| -------- | ------- | ------- |
| factory method  |     |   [factory method](https://refactoring.guru/design-patterns/factory-method)  |
| Abstract factory |      | [abstract factory](https://refactoring.guru/design-patterns/abstract-factory) |


## Books and Links

* Gang of Four
* [Design Patterns: Elements of Reusable Object-Oriented Software](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
* [refactoring improving the design of existing code](https://www.amazon.com/Refactoring-Improving-Design-Existing-Code/dp/0201485672)
* [Refacotoring Guru](https://refactoring.guru/) , similar to [Sourcemaking](https://sourcemaking.com/) ?