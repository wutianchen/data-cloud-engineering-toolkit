# Pattern: Observer

https://www.youtube.com/watch?v=oNalXg67XEE&list=PLC0nd42SBTaNuP4iB4L6SJlMaHE71FG6N&index=9


* listener
* event system


libraries: 
1. python event system: https://stackoverflow.com/questions/1092531/which-python-packages-offer-a-stand-alone-event-system
2. celery: https://docs.celeryq.dev/en/stable/getting-started/introduction.html

