# How to show real hexcode of a string

sometimes two strings look the same but not really the same https://stackoverflow.com/questions/45124237/strings-look-the-same-but-not-equal-in-bash

you can use hexdump to reveal them

for example:

```bash
printf true | hexdump -C
```

# xargs

Output piped to xargs will be placed at the end of the command passed to xargs. 

```bash
> echo "Bravo" | xargs echo "Alpha Charlie"
Alpha Charlie Bravo
```

This can be problematic when we want the output to go in the middle of the command. xargs has the facility for substituion however. Indicate the symbol or string you would like to replace with the -I flag

```bash
> echo "Bravo" | xargs -I SUB echo "Alpha SUB Charlie"
Alpha Bravo Charlie
```
of course, you can use the symbol multiple times

```bash
> echo "Bravo" | xargs -I SUB echo "Alpha SUB Charlie, SUB"
Alpha Bravo Charlie, Bravo
```
