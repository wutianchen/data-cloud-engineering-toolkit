# data-cloud-engineering-toolkit


## General Toolkit

* architectural decision records (ADRs): https://adr.github.io/


## Continous Learning

 * [leetcode](https://leetcode.com/)
 

## Knowledge Base

* [understanding json schema](https://json-schema.org/understanding-json-schema/index.html)
* [All You Need to Know about the Golang-based `Task` Runner](https://medium.com/toyota-connected-india/all-you-need-to-know-about-the-golang-based-task-runner-54d9675d5279)
* [Taskfile or GNU make - the battle of automation tools. Let's make the developer's life easier!](https://tsh.io/blog/taskfile-or-gnu-make-for-automation/)