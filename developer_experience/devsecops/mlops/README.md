# MLOps

* applied-ml: https://github.com/eugeneyan/applied-ml
* awesome-production-machine-learning: https://github.com/EthicalML/awesome-production-machine-learning (https://github.com/EthicalML)
* awesome mlops: https://github.com/visenger/awesome-mlops

* pycaret: https://pycaret.org/
* weights and biases: https://wandb.ai/site
* evidently: https://mlops.community/learn/monitoring/evidentlyai/

### Automated Feature Generation

* https://github.com/alteryx/featuretools
* https://towardsdatascience.com/automated-feature-engineering-in-python-99baf11cc219
* https://innovation.alteryx.com/deep-feature-synthesis/


### Netflix Metaflow

* [Unbundling Data Science Workflows with Metaflow and AWS Step Functions](https://netflixtechblog.com/unbundling-data-science-workflows-with-metaflow-and-aws-step-functions-d454780c6280 )
* [MLOps — Building a Production Ready Data Science Workflow management](https://medium.com/practical-data-science-and-engineering/mlops-building-a-production-ready-data-science-workflow-management-9c5cb6cab3d)

