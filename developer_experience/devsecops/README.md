# DevSecOps

Toolkit

| Tool              | Description | Others(Category, Links) |
| :---------------- | :------: | ----: |
| clair  |  static analysis of vulnerabilities in application containers   | https://github.com/quay/clair |
| trivy  |  a comprehensive and versatile security scanner    | https://github.com/aquasecurity/trivy |
| sops  |  secrets operations    | https://github.com/getsops/sops |


## Security Management

* [awesome chatops](https://github.com/exAspArk/awesome-chatops)
* Fass-Nomand: https://github.com/hashicorp/faas-nomad
* Serverless Stack: https://sst.dev/chapters/cross-stack-references-in-serverless.html


## Release Management

* standard-version: https://www.libhunt.com/r/standard-version
* release-please: https://github.com/googleapis/release-please
* conventional-changelog/standard-version: https://github.com/conventional-changelog/standard-version


## Dev Workflow

https://trunkbaseddevelopment.com/

## Ops Workflow

* https://codefresh.io/blog/stop-using-branches-deploying-different-gitops-environments/
* https://codefresh.io/blog/how-to-model-your-gitops-environments-and-promote-releases-between-them/
* https://codefresh.io/blog/how-to-structure-your-argo-cd-repositories-using-application-sets/
* https://codefresh.io/blog/how-to-structure-your-argo-cd-repositories-using-application-sets/