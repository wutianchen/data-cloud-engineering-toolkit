# jq (Json query)

https://github.com/stedolan/jq
https://jqplay.org/

* [Reshaping JSON with jq](https://programminghistorian.org/en/lessons/json-and-jq)
* [Project jq page](https://stedolan.github.io/jq/)
* [Manual jq](https://thoughtbot.com/blog/jq-is-sed-for-json)
* [jq is sed for JSON](https://thoughtbot.com/blog/jq-is-sed-for-json)
* [bash that JSON](https://blog.appoptics.com/jq-json/)
* [Parsing JSON with jq](http://www.compciv.org/recipes/cli/jq-for-parsing-json/)
* [JSON Lines](https://jsonlines.org/)
* [How to love jsonl - using JSON lines in your workflow](https://galea.medium.com/how-to-love-jsonl-using-json-line-format-in-your-workflow-b6884f65175b)
