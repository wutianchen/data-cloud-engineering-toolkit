# Parquet

Twitter and Cloudera merges efforts to develop columnar formats 

* [Efficient Data Storage for Analytics with Parquet 2.0](https://www.youtube.com/watch?v=MZNjmfx4LMc)
* [Dremel made simple with Parquet](https://blog.twitter.com/engineering/en_us/a/2013/dremel-made-simple-with-parquet)

Parquet Index Feature:

Page Index can be complementary to partitioning. For example, you can partition your data by a set of columns, and then use SORT BY to sort your data by another column that you use in the filtering criteria.

pyArrow

* [Apache Arrow: The Hidden Champion of Data Analytics](https://maximilianmichels.com/2020/apache-arrow-the-hidden-champion/)
* [Apache Arrow and the Future of Data Frames" with Wes McKinney](https://www.youtube.com/watch?v=fyj4FyH3XdU)
* [Extending Pandas using Apache Arrow and Numba - Uwe L  Korn](https://wutianchen.atlassian.net/wiki/spaces/DATA/pages/10978355/Data+Storage)

Apache Arrow: Read DataFrame With Zero Memory: https://towardsdatascience.com/apache-arrow-read-dataframe-with-zero-memory-69634092b1a
Extending Pandas using Apache Arrow and Numba - Uwe L Korn: https://www.youtube.com/watch?v=tvmX8YAFK80