# Architecture


## Data Mesh

#### Concept

Zhamak Dehghani of Thoughtworks, who coined the term data mesh in 2019

* https://aws.amazon.com/blogs/big-data/design-a-data-mesh-architecture-using-aws-lake-formation-and-aws-glue/ (https://aws.amazon.com/blogs/architecture/lets-architect-modern-data-architectures/)
* [Data Mesh Principles and Logical Architecture (by Zhamak Dehghani)](https://martinfowler.com/articles/data-mesh-principles.html)
* [Data Mesh: Get value from data at scale](https://www.thoughtworks.com/en-de/what-we-do/data-and-ai/data-mesh)

#### Use Case

*[How JPMorgan Chase built a data mesh architecture to drive significant value to enhance their enterprise data platform](https://aws.amazon.com/blogs/big-data/how-jpmorgan-chase-built-a-data-mesh-architecture-to-drive-significant-value-to-enhance-their-enterprise-data-platform/)

## Data Lakehouse

databricks

## Data Lake

tbd
