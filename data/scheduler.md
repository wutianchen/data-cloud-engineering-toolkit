# Scheduler

* stepfunction
* airflow
* prefect



## Airflow

tbd

## Prefect

* [prefect blog](https://www.prefect.io/guide/)
* [prefect blog on medium](https://medium.com/the-prefect-blog)
* [API Reference v1](https://docs-v1.prefect.io/api/latest/)
* [Documentation v1](https://docs-v1.prefect.io/) (there are two sections in it: core and orchastration)
