# State

Embedded database - Rocksdb

## Reference

* [DropBox Engineering Evening on RocksDB with Dhruba Borthakur @ Rockset](https://www.youtube.com/watch?v=aKAJMd0iKtI&list=PLhQS7ozMNQyZb-nbsa8Ak0Ttz7K3zXSyB)
* [State Unlocked - Seth Wiesman & Tzu-Li (Gordon) Tai](https://www.youtube.com/watch?v=Vjt_ofOSHps)
* [Managing State in Apache Flink - Tzu-Li (Gordon) Tai](https://www.youtube.com/watch?v=euFMWFDThiE)