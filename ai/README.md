# Data Science & AI

## AI

| Product    | Maker | Description |
| -------- | ------- | ------- |
| [Sora](https://openai.com/index/sora/)  | OpenAI    | text to image and video tool    |




* [open AI](https://openai.com/)
* [midjourney](https://www.midjourney.com/home/?callbackUrl=%2Fapp%2F)
* [deepmind](https://www.deepmind.com/)
* google
* [metabob](https://metabob.com/index.html)


tools

* [ChatGPT](https://chat.openai.com/chat)
* [ChatGPT Demo](https://gpt3demo.com/apps/openai-gpt-3-playground)
* [Github Copilot](https://github.com/features/copilot)
* [Bard](https://bard.google.com/)
* [autoGPT](https://github.com/Significant-Gravitas/Auto-GPT)
* [segment anything](https://ai.facebook.com/research/publications/segment-anything/)


## Open Data

* Registry of Open Data on AWS: https://registry.opendata.aws/
* OpenWeather global services: https://openweathermap.org/
